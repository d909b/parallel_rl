from application import Application
import sys

if __name__ == "__main__":
    import site
    print site.getsitepackages()
    app = Application(sys.argv)
    app.run()