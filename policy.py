import theano
import numpy as np
from util import NOPTS


class Policy():
    def act(self, state, is_training):
        raise NotImplementedError


class RandomPolicy(Policy):
    def __init__(self, action_space):
        self.action_space = action_space

    def act(self, state, is_training):
        return self.action_space.sample(), {}


class StochasticPolicy(Policy):
    def __init__(self, distribution):
        self.distribution = distribution

    @property
    def underlying_distribution(self):
        return self.distribution


class NeuralNetworkStochasticPolicy(StochasticPolicy):
    def __init__(self, distribution, neural_network):
        StochasticPolicy.__init__(self, distribution)
        self.neural_network = neural_network
        self.predict = theano.function([self.neural_network.input],
                                       self.neural_network.output,
                                       **NOPTS)

    def act(self, state, is_training):
        prob = self.predict(state[np.newaxis])
        distribution = self.underlying_distribution

        # We sample during training but always take the best action during evaluation
        # (Corresponds to no exploration)
        if is_training:
            return distribution.sample(prob)[0], {"p": prob[0]}
        else:
            return distribution.max_value(prob)[0], {"p": prob[0]}
