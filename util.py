from tabulate import tabulate
import scipy
import numpy as np
import theano
import theano.tensor as T

NOPTS = {
    "allow_input_downcast": True,
    "on_unused_input": 'ignore'
}


class DataTable():
    def __init__(self, initial_data=None):
        self.data = initial_data or {}

    def as_matrix(self):
        return np.column_stack((np.array(v) for v in self.data.values()))

    def get_data(self):
        return self.data

    def record_multiple(self, dictionary):
        for key, value in dictionary.iteritems():
            self.record(key, value)

    def record(self, key, value, full_replace=False):
        if full_replace:
            self.data[key] = value
            return

        if key not in self.data:
            self.data[key] = [None]*len(self.data.values()[0]) if len(self.data.values()) != 0 else [None]

        self.data[key][-1] = value

    def advance(self):
        for key in self.data.keys():
            self.data[key].append(None)

    def report(self, logger=None):
        # Note: Tabulate is very slow with large data tables - only call report for debugging purposes.
        text = tabulate(map(lambda l: map(lambda ll: "Multiple Values" if isinstance(ll, list) or isinstance(ll, dict)
                                                     else ll, l), zip(*self.data.values())), headers=self.data.keys())
        if logger is None:
            print text
        else:
            logger.info(text)

    def get_num_rows(self):
        n = 0
        if self.data is not None and len(self.data) != 0:
            n = len(self.data.values()[0])

        return n

    def get_dict_list(self):
        return [{key: value[index] for key, value in self.data.items()} for index in range(len(self.data.values()[0]))]


def merge_opts(l_opts, r_opts):
    """
    Merges r_opts into l_opts overriding l_opts values only if they don't already exist. Used to merge command line
    parameters with default settings.

    :param l_opts:
    :param r_opts:
    :return: Merged dict
    """
    return dict(r_opts.items() + dict(l_opts).items())


def explained_variance(ypred, y):
    """
    Computes fraction of variance that ypred explains about y.
    Returns 1 - Var[y-ypred] / Var[y]
    interpretation:
        ev=0  =>  might as well have predicted zero
        ev=1  =>  perfect prediction
        ev<0  =>  worse than just predicting zero
    """
    assert y.ndim == 1 and ypred.ndim == 1
    vary = np.var(y)
    return np.nan if vary == 0 else 1 - np.var(y-ypred)/vary


def explained_variance_2d(ypred, y):
    assert y.ndim == 2 and ypred.ndim == 2
    vary = np.var(y, axis=0)
    out = 1 - np.var(y-ypred)/vary
    out[vary < 1e-10] = 0
    return out


def flat_gradient(loss, var_list):
    grads = T.grad(loss, var_list)
    return T.concatenate([g.flatten() for g in grads])


def zipsame(*seqs):
    L = len(seqs[0])
    assert all(len(seq) == L for seq in seqs[1:])
    return zip(*seqs)


class SetFromFlat(object):
    def __init__(self, var_list):
        theta = T.vector()
        start = 0
        updates = []

        for v in var_list:
            shape = v.shape
            size = T.prod(shape)
            updates.append((v, theta[start:start+size].reshape(shape)))
            start += size

        self.op = theano.function([theta], [], updates=updates, **NOPTS)

    def __call__(self, theta):
        self.op(theta.astype(theano.config.floatX))


class GetFlat(object):
    def __init__(self, var_list):
        self.op = theano.function([], T.concatenate([v.flatten() for v in var_list]), **NOPTS)

    def __call__(self):
        return self.op()


class EzFlat(object):
    def __init__(self, var_list):
        self.gf = GetFlat(var_list)
        self.sff = SetFromFlat(var_list)

    def set_params_flat(self, theta):
        self.sff(theta)

    def get_params_flat(self):
        return self.gf()


# http://www.johndcook.com/blog/standard_deviation/
class RunningStat(object):
    def __init__(self, shape):
        self._n = 0
        self._M = np.zeros(shape)
        self._S = np.zeros(shape)

    def push(self, x):
        x = np.asarray(x)
        assert x.shape == self._M.shape
        self._n += 1
        if self._n == 1:
            self._M[...] = x
        else:
            oldM = self._M.copy()
            self._M[...] = oldM + (x - oldM)/self._n
            self._S[...] = self._S + (x - oldM)*(x - self._M)

    @property
    def n(self):
        return self._n

    @property
    def mean(self):
        return self._M

    @property
    def var(self):
        return self._S/(self._n - 1) if self._n > 1 else np.square(self._M)

    @property
    def std(self):
        return np.sqrt(self.var)

    @property
    def shape(self):
        return self._M.shape

    def set_parameters(self, parameters):
        self._n = parameters["_n"]
        self._M = parameters["_M"]
        self._S = parameters["_S"]

    def get_parameters(self):
        return {"_n": self._n, "_M": self._M, "_S": self._S}


class ZFilter(object):
    """
    y = (x-mean)/std
    using running estimates of mean,std
    """
    def __init__(self, shape, demean=True, destd=True, clip=10.0):
        self.demean = demean
        self.destd = destd
        self.clip = clip

        self.rs = RunningStat(shape)

    def __call__(self, x, update=True):
        if update:
            self.rs.push(x)

        if self.demean:
            x = x - self.rs.mean

        if self.destd:
            x /= (self.rs.std+1e-8)

        if self.clip:
            x = np.clip(x, -self.clip, self.clip)

        return x

    def output_shape(self, input_space):
        return input_space.shape

    def set_parameters(self, parameters):
        self.rs.set_parameters(parameters)

    def get_parameters(self):
        return self.rs.get_parameters()


def make_filters(configuration, observation_space):
    if configuration["--filter"]:
        state_filter = ZFilter(observation_space.shape, clip=5)
        reward_filter = ZFilter((), demean=False, clip=10)
    else:
        state_filter = lambda x: x
        reward_filter = lambda x: x

    return state_filter, reward_filter


def save_parameters(filename, parameters):
    np.savez(filename, **parameters)


def load_parameters(filename):
    data = np.load(filename)

    parameters = {}
    for name in data.files:
        parameters[name] = data[name]
    return parameters
