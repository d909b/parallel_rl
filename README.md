# Parallel Reinforcement Learning Framework

Prototype of a scalable reinforcement learning framework built on top of [ J. Schulman's Modular_RL ](https://github.com/joschu/modular_rl). The framework currently only supports random and Trust Region Policy Optimisation (TRPO) agents.

## Architecture

Inspired by [Nair et al. "Massively Parallel Methods for Deep Reinforcement Learning", 2015](https://arxiv.org/pdf/1507.04296.pdf)

## Dependencies 
* Python 2.7
* SciPy
* NumPy
* Keras, with
* Theano backend
* Apache Spark (Only necessary for the cluster version)