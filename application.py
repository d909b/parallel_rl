import gym
import os
import sys
import getopt
import agent as agents
from util import DataTable
import tempfile
from gym.envs import registry
import numpy as np
import itertools
import time
import logging
from util import merge_opts, save_parameters, load_parameters
from data_processing import split_paths

OPTS = {
    "--num_iterations": 1,
    "--timesteps_per_iteration": 25,
    "--seed": 0,
    "--out_dir": tempfile.gettempdir(),
    "--environment": "Pendulum-v0",
    "--max_path_length": 0
}


class Application():
    @staticmethod
    def print_usage():
        print("Sample usage: python agent.py (--help) (--environment CartPole-v0) (--num_iterations=250)"
              " (--timesteps_per_iteration=100) (--show_video) (--out_file=./output) (--debug) (--filter)")
        print("---")
        print("Available environments are: ")
        print([spec.id for spec in registry.all()])

    def print_directory_inexistant(self, directory):
        self.logger.error("Directory [" + directory + "] does not exist.")

    @staticmethod
    def parse_args(argv):
        try:
            opts, _ = getopt.getopt(argv[1:], "",
                                    ["help", "environment=",
                                     "num_iterations=", "timesteps_per_iteration=",
                                     "show_video", "out_dir=",
                                     "seed=", "debug", "filter", "snapshot=", "load_parameters="])
        except getopt.GetoptError:
            Application.print_usage()
            sys.exit(2)

        return merge_opts(opts, OPTS)

    def __init__(self, argv, monitored=True, parameters=None):
        self.logger = logging.getLogger(__name__)
        self.statistics = DataTable()
        self.monitored = monitored
        self.snapshot = 0
        self.directory = None
        self.max_path_length = None
        self.parameters_file = "parameters"
        self.show_video = False
        self.debug = False

        # Parse command line arguments
        opts = Application.parse_args(argv)
        opts = list(opts.items())

        for opt, arg in opts:
            if opt is "--help":
                Application.print_usage()
                sys.exit()
            elif opt == "--environment":
                environment = arg
            elif opt == "--num_iterations":
                self.num_iterations = int(arg)
            elif opt == "--timesteps_per_iteration":
                self.timesteps_per_iteration = int(arg)
            elif opt == "--show_video":
                self.show_video = True
            elif opt == "--out_dir":
                self.logger.info("Output is saved to [" + arg + "]")
                self.directory = arg
            elif opt == "--seed":
                random_seed = int(arg)
            elif opt == "--max_path_length":
                self.max_path_length = int(arg)
            elif opt == "--debug":
                self.debug = True
            elif opt == "--load_parameters":
                parameters = self.parameters_file = arg
            elif opt == "--filter":
                self.logger.warn("--f is currently not implemented.")
            elif opt == "--snapshot":
                self.snapshot = int(arg)

        # Global setup
        np.set_printoptions(precision=4, suppress=True)
        np.random.seed(random_seed)
        gym.logger.setLevel(gym.logging.INFO if self.debug else gym.logging.WARN)
        self.logger.setLevel(logging.INFO if self.debug else logging.WARN)
        self.record_episode = False

        # Create environment
        self.current_gym = gym.make(environment)

        if self.monitored or self.snapshot != 0:
            # Create output directory
            if not os.path.exists(self.directory):
                os.mkdir(self.directory)

        if self.monitored:
            self.current_gym.monitor.start(self.directory, force=True, video_callable=lambda x: self.record_episode)

        opts = dict(opts)
        if self.max_path_length == 0:
            opts["--max_path_length"] = self.max_path_length = self.current_gym.spec.timestep_limit - 1

        if isinstance(self.current_gym.action_space, gym.spaces.Box):
            num_actions = self.current_gym.action_space.shape[0]
        else:
            num_actions = self.current_gym.action_space.n

        if isinstance(self.current_gym.action_space, gym.spaces.Box):
            space_type = 'Box'
        else:
            space_type = 'Discrete'

        self.logger.info("Action space is " + str(space_type))
        self.logger.info("Number of states: " + str(self.current_gym.observation_space.shape))
        self.logger.info("Number of actions: " + str(num_actions))
        self.logger.info("Options: " + str(opts))

        # Instantiate agent
        self.agent = agents.TRPOAgent(self.current_gym.observation_space, self.current_gym.action_space, opts)
        self.initialise_model_parameters(parameters)

    def initialise_model_parameters(self, parameters):
        is_error_case = isinstance(parameters, str) and not os.path.exists(parameters)
        if is_error_case:
            self.logger.error("Could not load parameter file at " + parameters + " because the file doesn't exist.")

        if parameters is not None and not is_error_case:
            if isinstance(parameters, str):
                self.logger.info("Loading existing parameters from file " + self.parameters_file)
                self.agent.set_parameters(load_parameters(self.parameters_file))
            else:
                self.logger.info("Loading existing parameters supplied argument.")
                self.agent.set_parameters(parameters)
        else:
            self.logger.info("Starting with clean model.")

    def run(self):
        """
        Performs num_iterations simulations and records the associated statistics.
        """
        seed_iter = itertools.count()
        for i in xrange(self.num_iterations):
            self.statistics.advance()

            start_time = time.time()
            data = self.simulate(seed_iter)
            self.statistics.record_multiple({"stats": data.get_data(),
                                             "time": time.time() - start_time})

            # Let the agent process the batch of experiences
            self.agent.experience(self.agent.preprocess_data(data.get_data()))

            self.on_iteration_end(i, data)

        if self.monitored:
            self.current_gym.monitor.close()

    def on_iteration_end(self, i, data):
        self.logger.info("Iteration " + str(i) + ":")

        # Prepare iteration statistics
        paths = split_paths(data.get_data())
        rewards = np.array([path["r"].sum() for path in paths])
        self.logger.info("Mean cumultative reward: " + str(rewards.mean()))

        if self.snapshot != 0 and i % self.snapshot == 0:
            save_parameters(self.parameters_file, self.agent.get_parameters())
            self.logger.info("Saved snapshot.")

    def simulate(self, seed_iter=itertools.count()):
        """
        Performs a simulation from the initial state until either a terminal state is reached or the agent
        forces an environment reset.

        :return: A table containing simulation data of size [n_items x timesteps_per_iteration]
        """
        state = self.agent.state_filter(self.current_gym.reset())
        stats = DataTable()

        # Simulate up to max_timesteps in a single run and record all the experiences (s,a,r,s')
        for t in xrange(self.timesteps_per_iteration):
            stats.advance()

            _, action, reward, next_state, done, agent_reset, agent_data = self.step(state)

            # Agents can choose to pre-filter states
            next_state = self.agent.state_filter(next_state)

            stats.record_multiple(merge_opts({
                "s": state, "a": action, "r": reward, "s+": next_state,
                "done": done, "reset": agent_reset
            }, agent_data))
            state = next_state

            if done or agent_reset:
                np.random.seed(seed_iter.next())
                state = self.current_gym.reset()

        return stats

    def step(self, state):
        """
        Performs a single step in the environment and reports the resulting (s,a,r,s',done,reset) tuple.

        :param state: The initial environment state
        :return: The resulting information tuple
        """
        if self.show_video:
            self.current_gym.render()

        action, agent_reset, agent_data = self.agent.select_action(state, is_training=True)
        next_state, reward, done, info = self.current_gym.step(action)

        return state, action, reward, next_state, done, agent_reset, agent_data