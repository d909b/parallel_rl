import numpy as np
import theano
import theano.tensor as T
from collections import OrderedDict
from util import merge_opts, flat_gradient, zipsame, NOPTS, EzFlat

OPTS = {
    "--cg_damping": 0.1,
    "--max_kl": 0.01
}


class TRPOUpdater(EzFlat):
    def __init__(self, nn, distribution, configuration):
        configuration = merge_opts(configuration, OPTS)

        self.distribution = distribution
        self.cg_damping = configuration["--cg_damping"]
        self.max_kl = configuration["--max_kl"]

        params = nn.trainable_weights
        EzFlat.__init__(self, params)

        ob_no = nn.input
        act_na = self.distribution.sampled_variable()
        adv_n = T.vector("adv_n")

        # Probability distribution:
        prob_np = nn.output
        oldprob_np = distribution.probabilistic_variable()

        logp_n = distribution.loglikelihood(act_na, prob_np)
        oldlogp_n = distribution.loglikelihood(act_na, oldprob_np)
        n = ob_no.shape[0]

        # Policy gradient:
        surr = (-1.0 / n) * T.exp(logp_n - oldlogp_n).dot(adv_n)
        pg = flat_gradient(surr, params)

        prob_np_fixed = theano.gradient.disconnected_grad(prob_np)
        kl_firstfixed = distribution.kl_divergence(prob_np_fixed, prob_np).sum() / n

        grads = T.grad(kl_firstfixed, params)
        flat_tangent = T.fvector(name="flat_tan")
        shapes = [var.get_value(borrow=True).shape for var in params]

        start = 0
        tangents = []
        for shape in shapes:
            size = np.prod(shape)
            tangents.append(T.reshape(flat_tangent[start:start+size], shape))
            start += size

        gvp = T.add(*[T.sum(g*tangent) for (g, tangent) in zipsame(grads, tangents)])

        # Fisher-vector product
        fvp = flat_gradient(gvp, params)

        ent = distribution.entropy(prob_np).mean()
        kl = distribution.kl_divergence(oldprob_np, prob_np).mean()

        losses = [surr, kl, ent]
        self.loss_names = ["surr", "kl", "ent"]

        args = [ob_no, act_na, adv_n, oldprob_np]

        self.compute_policy_gradient = theano.function(args, pg, **NOPTS)
        self.compute_losses = theano.function(args, losses, **NOPTS)
        self.compute_fisher_vector_product = theano.function([flat_tangent] + args, fvp, **NOPTS)

    def update(self, data):
        s = data["s"]
        p = data["p"]
        a = data["a"]
        advantage = data["advantage"]

        args = (s, a, advantage, p)

        th_prev = self.get_params_flat()

        def fisher_vector_product(prob):
            return self.compute_fisher_vector_product(prob, *args) + self.cg_damping*prob

        g = self.compute_policy_gradient(*args)
        losses_before = self.compute_losses(*args)

        if np.allclose(g, 0):
            print "got zero gradient. not updating"
        else:
            stepdir = cg(fisher_vector_product, -g)
            shs = .5*stepdir.dot(fisher_vector_product(stepdir))
            lm = np.sqrt(shs / self.max_kl)

            print "lagrange multiplier:", lm, "gnorm:", np.linalg.norm(g)

            fullstep = stepdir / lm
            neggdotstepdir = -g.dot(stepdir)

            def loss(th):
                self.set_params_flat(th)
                return self.compute_losses(*args)[0]

            success, theta = linesearch(loss, th_prev, fullstep, neggdotstepdir / lm)
            print "success", success

            if success:
                self.set_params_flat(theta)
            else:
                self.set_params_flat(th_prev)

        losses_after = self.compute_losses(*args)

        out = OrderedDict()

        for (lname, lbefore, lafter) in zipsame(self.loss_names, losses_before, losses_after):
            out[lname+"_before"] = lbefore
            out[lname+"_after"] = lafter

        return out


def linesearch(f, x, fullstep, expected_improve_rate, max_backtracks=10, accept_ratio=.1):
    """
    Backtracking linesearch, where expected_improve_rate is the slope dy/dx at the initial point
    """
    fval = f(x)
    print "fval before", fval
    for (_n_backtracks, stepfrac) in enumerate(.5**np.arange(max_backtracks)):
        xnew = x + stepfrac*fullstep
        newfval = f(xnew)
        actual_improve = fval - newfval
        expected_improve = expected_improve_rate*stepfrac
        ratio = actual_improve/expected_improve
        print "a/e/r", actual_improve, expected_improve, ratio
        if ratio > accept_ratio and actual_improve > 0:
            print "fval after", newfval
            return True, xnew
    return False, x


def cg(f_Ax, b, cg_iters=10, callback=None, verbose=False, residual_tol=1e-10):
    """
    Demmel p 312
    """
    p = b.copy()
    r = b.copy()
    x = np.zeros_like(b)
    rdotr = r.dot(r)

    fmtstr = "%10i %10.3g %10.3g"
    titlestr = "%10s %10s %10s"

    if verbose:
        print titlestr % ("iter", "residual norm", "soln norm")

    for i in xrange(cg_iters):
        if callback is not None:
            callback(x)

        if verbose:
            print fmtstr % (i, rdotr, np.linalg.norm(x))

        z = f_Ax(p)
        v = rdotr / p.dot(z)
        x += v*p
        r -= v*z
        newrdotr = r.dot(r)
        mu = newrdotr/rdotr
        p = r + mu*p
        rdotr = newrdotr

        if rdotr < residual_tol:
            break

    if callback is not None:
        callback(x)

    if verbose:
        print fmtstr % (i + 1, rdotr, np.linalg.norm(x))

    return x