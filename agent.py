import gym.spaces
import distribution as distributions
import policy as policies
from nn_model import NeuralNetworkModel
from util import merge_opts, make_filters
from data_processing import standardise_advantages, enrich_data
from trpo_update import TRPOUpdater


class Agent():
    OPTS = {
        "--filter": 1
    }

    def __init__(self, observation_space, action_space, policy, configuration):
        configuration = merge_opts(configuration, Agent.OPTS)
        self.observation_space = observation_space
        self.action_space = action_space
        self.configuration = configuration
        self.policy = policy
        self._state_filter, self._reward_filter = make_filters(configuration, observation_space)

    def select_action(self, state, is_training):
        action, policy_data = self.policy.act(state, is_training)
        return action, False, policy_data

    def experience(self, data):
        """
        Notifies the agent of the outcome of her selected actions in form of a data table consisting of experience
        tuples.

        :param data: A data table containing an arbitrary number of experience tuples.
        """
        pass

    def get_parameters(self):
        pass

    def set_parameters(self, parameters):
        pass

    @property
    def state_filter(self):
        return self._state_filter

    @property
    def reward_filter(self):
        # TODO: Currently unused.
        return self._reward_filter


class RandomAgent(Agent):
    def __init__(self, observation_space, action_space, configuration):
        Agent.__init__(self, observation_space, action_space, policies.RandomPolicy(action_space), configuration)


class StochasticNNAgent(Agent):
    def __init__(self, observation_space, action_space, configuration):
        if isinstance(action_space, gym.spaces.Box):
            distribution = distributions.DiagonalGaussianDistribution(action_space.shape[0])
        else:
            distribution = distributions.CategoricalDistribution(action_space.n)

        nn, _ = NeuralNetworkModel.make_stochastic(observation_space, action_space, configuration)
        self.value_function = NeuralNetworkModel.make_value_network(observation_space, action_space, configuration)

        Agent.__init__(self, observation_space, action_space,
                       policies.NeuralNetworkStochasticPolicy(distribution, nn), configuration)


class TRPOAgent(StochasticNNAgent):
    OPTS = {
        "--max_path_length": 0,
        "--gamma": 0.995,
        "--lambda": 0.97
    }

    def __init__(self, observation_space, action_space, configuration):
        configuration = merge_opts(configuration, TRPOAgent.OPTS)
        StochasticNNAgent.__init__(self, observation_space, action_space, configuration)

        self.max_path_length = configuration["--max_path_length"]
        self.current_path_length = 0
        self.gamma = configuration["--gamma"]
        self.lam = configuration["--lambda"]
        self.updater = TRPOUpdater(self.policy.neural_network, self.policy.underlying_distribution, configuration)

    def select_action(self, state, is_training):
        action, _, data = StochasticNNAgent.select_action(self, state, is_training)

        self.current_path_length += 1
        done = self.current_path_length >= self.max_path_length
        if done:
            self.current_path_length = 0

        # Reset if max path length was exceeded
        return action, done, data

    def preprocess_data(self, data):
        return enrich_data(data, gamma=self.gamma, lam=self.lam, timestep_limit=self.max_path_length,
                           value_function=lambda states, time_points: self.value_function.predict(states, time_points))

    def experience(self, data):
        standardise_advantages(data)

        # Update value function NN
        self.value_function.fit(data)

        # Update policy
        self.updater.update(data)

        return self.updater.get_params_flat(), self.value_function.reg.opt.get_params_flat()

    def get_parameters(self):
        return merge_opts({"policy": self.updater.get_params_flat(),
                           "value": self.value_function.reg.opt.get_params_flat()},
                          self.state_filter.get_parameters())

    def set_parameters(self, parameters):
        self.updater.set_params_flat(parameters["policy"])
        self.value_function.reg.opt.set_params_flat(parameters["value"])
        self.state_filter.set_parameters(parameters)