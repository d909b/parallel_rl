import gym
import gym.spaces
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense
from keras.layers.core import Layer
import theano
import theano.tensor as T
from util import merge_opts, explained_variance, explained_variance_2d, NOPTS, flat_gradient, EzFlat
from collections import OrderedDict
import scipy.optimize

OPTS = {
    "--layer_sizes": "64,64",
    "--activation": "tanh"
}


class NeuralNetworkModel():
    @staticmethod
    def make_stochastic(observation_space, action_space, configuration):
        assert isinstance(observation_space, gym.spaces.Box)

        configuration = merge_opts(configuration, OPTS)

        if isinstance(action_space, gym.spaces.Box):
            num_ouputs = action_space.shape[0]
        else:
            num_ouputs = action_space.n

        layer_sizes = map(int, configuration["--layer_sizes"].split(','))
        activation = configuration["--activation"]

        nn = Sequential()
        for (i, num_outputs_last) in enumerate(layer_sizes):
            input_shape = dict(input_shape=observation_space.shape) if i == 0 else {}
            nn.add(Dense(num_outputs_last, activation=activation, **input_shape))

        if isinstance(action_space, gym.spaces.Box):
            nn.add(Dense(num_ouputs))
            w_last = nn.layers[-1].W
            w_last.set_value(w_last.get_value(borrow=True)*0.1)
            nn.add(ConcatFixedStd())
        else:
            nn.add(Dense(num_ouputs, activation="softmax"))
            w_last = nn.layers[-1].W
            w_last.set_value(w_last.get_value(borrow=True)*0.1)

        return nn, num_ouputs

    @staticmethod
    def make_value_network(observation_space, action_space, configuration):
        configuration = merge_opts(configuration, OPTS)

        layer_sizes = map(int, configuration["--layer_sizes"].split(','))
        activation = configuration["--activation"]

        nn = Sequential()

        for (i, num_outputs_last) in enumerate(layer_sizes):
            # add one extra feature for timestep
            input_shape = dict(input_shape=(observation_space.shape[0] + 1,)) if i == 0 else {}
            nn.add(Dense(num_outputs_last, activation=activation, **input_shape))

        nn.add(Dense(1))

        return NeuralNetworkValueFunction(nn, dict(mixfrac=0.1))


class ConcatFixedStd(Layer):
    def __init__(self, **kwargs):
        Layer.__init__(self, **kwargs)
        self.log_std = None

    def build(self, input_shape):
        input_dim = input_shape[1]
        self.log_std = theano.shared(np.zeros(input_dim, theano.config.floatX), name='{}_logstd'.format(self.name))
        self.trainable_weights = [self.log_std]

    def get_output_shape_for(self, input_shape):
        return input_shape[0], input_shape[1] * 2

    def call(self, x, mask=None):
        mean = x
        std = T.repeat(T.exp(self.log_std)[None, :], mean.shape[0], axis=0)
        return T.concatenate([mean, std], axis=1)


class NeuralNetworkValueFunction(object):
    def __init__(self, net, regression_params):
        self.reg = NNRegression(net, **regression_params)

    def predict(self, states, time_points):
        ob_no = self.preprocess(states, time_points)
        return self.reg.predict(ob_no)[:, 0]

    def fit(self, paths):
        ob_no = self.preprocess(paths["s"], paths["t"])
        vtarg_n1 = paths["return"].reshape(-1, 1)
        return self.reg.fit(ob_no, vtarg_n1)

    def preprocess(self, states, time_points):
        return np.concatenate([states, time_points], axis=1)


class NNRegression():
    def __init__(self, net, mixfrac=1.0, maxiter=25):
        self.net = net
        self.mixfrac = mixfrac

        x_nx = net.input
        self.predict = theano.function([x_nx], net.output, **NOPTS)

        ypred_ny = net.output
        ytarg_ny = T.matrix("ytarg")
        var_list = net.trainable_weights
        l2 = 1e-3 * T.add(*[T.square(v).sum() for v in var_list])

        n = x_nx.shape[0]
        mse = T.sum(T.square(ytarg_ny - ypred_ny))/n

        symb_args = [x_nx, ytarg_ny]
        loss = mse + l2

        self.opt = LBFGSOptimizer(loss, var_list, symb_args, maxiter=maxiter, extra_losses={"mse": mse, "l2": l2})

    def fit(self, x_nx, ytarg_ny):
        ny = ytarg_ny.shape[1]

        ypredold_ny = self.predict(x_nx)
        out = self.opt.update(x_nx, ytarg_ny*self.mixfrac + ypredold_ny*(1-self.mixfrac))
        yprednew_ny = self.predict(x_nx)

        out["PredStdevBefore"] = ypredold_ny.std()
        out["PredStdevAfter"] = yprednew_ny.std()
        out["TargStdev"] = ytarg_ny.std()

        if ny == 1:
            out["EV_before"] = explained_variance_2d(ypredold_ny, ytarg_ny)[0]
            out["EV_after"] = explained_variance_2d(yprednew_ny, ytarg_ny)[0]
        else:
            out["EV_avg"] = explained_variance(yprednew_ny.ravel(), ytarg_ny.ravel())

        return out


class LBFGSOptimizer(EzFlat):
    def __init__(self, loss,  params, symb_args, extra_losses=None, maxiter=25):
        EzFlat.__init__(self, params)
        self.all_losses = OrderedDict()
        self.all_losses["loss"] = loss

        if extra_losses is not None:
            self.all_losses.update(extra_losses)

        self.f_lossgrad = theano.function(list(symb_args), [loss, flat_gradient(loss, params)], **NOPTS)
        self.f_losses = theano.function(symb_args, self.all_losses.values(), **NOPTS)
        self.maxiter = maxiter

    def update(self, *args):
        th_prev = self.get_params_flat()

        def loss_and_gradient(th):
            l, g = self.f_lossgrad(*args)
            g = g.astype('float64')
            return l, g

        losses_before = self.f_losses(*args)
        theta, _, opt_info = scipy.optimize.fmin_l_bfgs_b(loss_and_gradient, th_prev, maxiter=self.maxiter)
        del opt_info['grad']

        losses_after = self.f_losses(*args)
        info = OrderedDict()

        for (name, loss_before, loss_after) in zip(self.all_losses.keys(), losses_before, losses_after):
            info[name+"_before"] = loss_before
            info[name+"_after"] = loss_after

        return info