import scipy
import numpy as np
from util import DataTable


def split_paths(data):
    """
    Splits the data table into distinct paths using the done and reset flags.
    """
    reset_or_done_idx = np.where(np.logical_or(data["reset"], data["done"]))[0] + 1

    if len(reset_or_done_idx) > 0 and reset_or_done_idx[-1] == len(data["reset"]):
        reset_or_done_idx = reset_or_done_idx[:-1]

    dl = {k: np.split(v, indices_or_sections=reset_or_done_idx) for k, v in data.items()}

    # Return value is a list of dicts/paths
    return [{key: value[index] for key, value in dl.items()} for index in range(len(dl.values()[0]))]


def discount(x, gamma):
    """
    computes discounted sums along 0th dimension of x.
    inputs
    ------
    x: ndarray
    gamma: float
    outputs
    -------
    y: ndarray with same shape as x, satisfying
        y[t] = x[t] + gamma*x[t+1] + gamma^2*x[t+2] + ... + gamma^k x[t+k],
                where k = len(x) - t - 1
    """
    return scipy.signal.lfilter([1], [1, -gamma], x[::-1], axis=0)[::-1]


def enrich_data(data, value_function, gamma, lam, timestep_limit):
    """
    Enriches __data__ to contain discounted advantage information and relative time points.

    :param data: An experience matrix with rows containing experience tuples.
    :return: The original experience matrix with an additional column of advantage scores.
    """
    paths = split_paths(data)

    assert len(paths) >= 1

    # Compute advantages and relative time points per path.
    for path in paths:
        # Compute time points first.
        path["t"] = np.concatenate([np.arange(len(path["s"])).reshape(-1, 1) / float(timestep_limit)], axis=1)

        path["return"] = discount(path["r"], gamma)
        baseline = path["baseline"] = value_function(path["s"], path["t"])
        did_terminate = path["done"][-1]
        baseline_terminated = np.append(baseline, 0 if did_terminate else baseline[-1])
        deltas = path["r"] + gamma * baseline_terminated[1:] - baseline_terminated[:-1]
        path["advantage"] = discount(deltas, gamma * lam)

    # Concatenate all paths.
    advantages = np.concatenate([path["advantage"] for path in paths])
    r = np.concatenate([path["return"] for path in paths])
    s = np.concatenate([path["s"] for path in paths])
    p = np.concatenate([path["p"] for path in paths])
    a = np.concatenate([path["a"] for path in paths])
    t = np.concatenate([path["t"] for path in paths])

    # Note: We don't standardise at this point.
    #       We'll have more meaningful mean / std estimates in the aggregated mini-batch.
    return {"return": r, "s": s, "p": p, "a": a, "t": t, "advantage": advantages}


def dict_as_tuples(d):
    dt = DataTable(d)
    return list(map(tuple, dt.as_matrix()))


def dict_as_dict_list(d):
    return DataTable(d).get_dict_list()


def dict_from_dict_list(dl):
    dt = DataTable()
    for d in dl:
        dt.record_multiple(d)
    return dt.get_data()


def dict_entries_as_array(d):
    return {k: np.asarray(v) for k, v in d.items()}


def standardise_advantages(data):
    advantage = data["advantage"]
    std = advantage.std()
    mean = advantage.mean()
    data["advantage"] = (advantage - mean) / std