from Queue import Full


class Actor():
    """
    Actors generate experience tuples by interacting with an environment using the current global policy network.
    The policy network(s) are updated at predefined sync points.

    See also: Nair et al. "Massively Parallel Methods for Deep Reinforcement Learning", 2015
    """
    def __init__(self, experience_queue, simulation_func):
        self.experience_queue = experience_queue
        self.simulation_func = simulation_func

    def run(self, terminated):
        while not terminated.value:
            # Run a simulation
            experience = self.simulation_func()

            # Report the result
            try:
                self.experience_queue.put(experience, block=False)
            except Full:
                continue