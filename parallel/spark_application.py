from application import Application
from local_singleton import Singleton
from data_processing import dict_as_dict_list, dict_from_dict_list, dict_entries_as_array
from util import merge_opts
import numpy as np
import sys
import logging

OPTS = {
    "--subdivision_size": 200
}


@Singleton
class NodeLocalApplication(Application):
    """
    A node-local wrapper for the application object.
    We use this wrapper to ensure that the Application object will be instantiated only once per worker node.
    (Since its instantiation is a very expensive operation)
    """
    def __init__(self):
        Application.__init__(self, sys.argv)


# Using currying to avoid binding to the __SparkApplication__ object
# which would incur serialisation and sharing across workers.
def collect_experience(subdivison_size):
    def inner_collect_experience(t):
        # Set the per-agent number of timesteps.
        NodeLocalApplication.instance().timesteps_per_iteration = subdivison_size
        data = NodeLocalApplication.instance().simulate()
        data = NodeLocalApplication.instance().agent.preprocess_data(data.get_data())
        return iter([("experience", t) for t in dict_as_dict_list(data)])
    return inner_collect_experience


def compute_gradients(items):
    # Transform the RDD tuples into the expected format.
    d = [tup[1] for tup in items]
    d = dict_from_dict_list(d)
    d = dict_entries_as_array(d)

    # Compute parameter gradient(s).
    return "gradient", NodeLocalApplication.instance().agent.experience(d)


class SparkApplication():
    def __init__(self, sc):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.sc = sc

        opts = Application.parse_args(sys.argv)
        opts = merge_opts(opts, OPTS)

        if "--load_parameters" in opts:
            self.parameters = opts["--load_parameters"]
        else:
            self.parameters = None

        if "--num_iterations" in opts:
            self.num_iterations = int(opts["--num_iterations"])

        if "--timesteps_per_iteration" in opts:
            self.timesteps_per_iteration = int(opts["--timesteps_per_iteration"])

        if "--subdivision_size" in opts:
            self.subdivision_size = min(int(opts["--subdivision_size"]), self.timesteps_per_iteration)
            self.num_partitions = self.timesteps_per_iteration / (1.0 * self.subdivision_size)
            self.num_partitions = int(np.ceil(self.num_partitions))
        else:
            self.num_partitions = 2

        if "--minibatch_size" in opts:
            self.minibatch_size = min(int(opts["--minibatch_size"]), self.timesteps_per_iteration)
        else:
            self.minibatch_size = min(50, self.timesteps_per_iteration)

        self.subdivision_size = int(np.ceil(self.timesteps_per_iteration / self.num_partitions))
        self.num_batches = int(np.ceil(self.timesteps_per_iteration / (1.0 * self.minibatch_size)))

        self.logger.info("Options: " + str(opts))
        self.logger.info("Num. partitions: {0}".format(self.num_partitions))
        self.logger.info("Num. mini batches: {0}".format(self.num_batches))

    def run(self):
        for i in xrange(self.num_iterations):
            # Collect and randomly shuffle experiences.
            res = self.sc.parallelize(xrange(self.timesteps_per_iteration), self.num_partitions)\
                .mapPartitions(collect_experience(self.subdivision_size), preservesPartitioning=True)\
                .randomSplit([1.0/self.num_partitions]*self.num_partitions)

            # Recombine the shuffled data into one RDD and compute gradients on each mini batch
            self.sc.union(res).repartition(self.num_batches)\
                .mapPartitions(compute_gradients, preservesPartitioning=True)

            # TODO: Reduce, apply and store updated parameter set.

