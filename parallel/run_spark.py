# This is an Apache Spark application.
# Must be executed with spark-submit.

from pyspark import SparkConf, SparkContext
from spark_application import SparkApplication

APP_NAME = "Spark Reinforcement Learning"


def main(ctx):
    app = SparkApplication(ctx)
    app.run()

if __name__ == "__main__":
    # Setup spark.
    cfg = SparkConf().setAppName(APP_NAME)
    cfg = cfg.setMaster("local[*]")
    sc = SparkContext(conf=cfg)

    main(sc)