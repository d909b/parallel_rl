from multiprocessing import Process, Queue, Value, Array, cpu_count
from distributed_memory import BatchingMemory
from parameter_server import ParameterServer
from actor import Actor
from learner import Learner
from application import Application
import sys
import signal
import ctypes


def start_replay_memory_server(experience_queue, batching_queue, batch_size, kill_flag):
    replay_memory = BatchingMemory(experience_queue, batching_queue, batch_size)
    replay_memory.run(kill_flag)


def start_parameter_server(gradient_queue, parameter_queue, parameters, num_parameter_consumers, kill_flag):
    parameter_server = ParameterServer(gradient_queue, parameter_queue, parameters, num_parameter_consumers)
    parameter_server.run(kill_flag)


def start_learner(batch_queue, gradient_queue, parameters, argv, kill_flag):
    app = Application(argv, monitored=False, parameters=parameters)

    def learn(batch):
        return app.agent.experience(batch)

    learner = Learner(batch_queue, gradient_queue, learn)
    learner.run(kill_flag)


def start_actor(experience_queue, parameters, argv, kill_flag):
    app = Application(argv, monitored=False, parameters=parameters)

    def run_simulation():
        return app.agent.preprocess_data(app.simulate())

    actor = Actor(experience_queue, run_simulation)
    actor.run(kill_flag)


def main(argv):
    global terminated

    def signal_handler(signal, frame):
        global terminated
        terminated.value = True
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # TODO: Expose these settings as command line parameters
    num_actors = cpu_count()
    num_learners = num_actors
    num_parameter_consumers = num_actors + num_learners + 2
    queue_size = 32
    batch_size = 200

    experience_queue = Queue(queue_size)
    batch_queue = Queue(queue_size)
    gradient_queue = Queue(queue_size)
    parameter_queue = Queue(num_parameter_consumers)

    app = Application()
    params = app.agent.get_parameters()

    # The parameters are a shared object that is synchronized between learners/actors after every episode
    parameters = Array(ctypes.py_object, len(params))
    for i, param in enumerate(params):
        parameters.value[i] = Array('d', param)

    # Construct all child processes:
    # 1 replay memory server, 1 parameter server, n learners and k actors
    memory_server = Process(target=start_replay_memory_server,
                            args=(experience_queue, batch_queue, batch_size, terminated))
    parameter_server = Process(target=start_parameter_server,
                               args=(gradient_queue, parameter_queue, parameters, num_parameter_consumers, terminated))

    processes = [memory_server, parameter_server]
    for _ in xrange(num_learners):
        processes.append(Process(target=start_learner,
                                 args=(batch_queue, gradient_queue, parameters, argv, terminated)))

    for _ in xrange(num_actors):
        processes.append(Process(target=start_actor,
                                 args=(experience_queue, parameters, argv, terminated)))

    # Start the child processes
    for p in processes:
        p.start()

    # Wait for their completion
    for p in processes:
        p.join()


if __name__ == "__main__":
    terminated = Value('i', False)
    main(sys.argv)