import numpy as np
from Queue import Empty, Full


class DistributedMemory():
    def __init__(self, queue):
        self.data = None
        self.queue = queue

    def run(self, terminated):
        while not terminated.value:
            try:
                self.record_data(self.queue.get(timeout=1))
            except Empty:
                continue

    def record_data(self, experience):
        if self.data is None:
            self.data = experience

        np.stack((self.data, experience))
        return experience.get_num_rows()

    def get_data(self):
        return self.data

    def clear(self):
        self.data = None


class BatchingMemory(DistributedMemory):
    def __init__(self, input_queue, output_queue, batch_size):
        DistributedMemory.__init__(self, input_queue)
        self.output_queue = output_queue
        self.batch_size = batch_size

    def run(self, terminated):
        while not terminated.value:
            try:
                self.record_data(self.queue.get(timeout=1).get_data())

                n = self.data.get_num_rows()

                while n >= self.batch_size and not terminated.value:
                    sample = self.sample_and_remove(self.batch_size)
                    n -= self.batch_size

                    while not terminated.value:
                        try:
                            self.output_queue.put(sample, timeout=1)
                        except Full:
                            continue
            except Empty:
                continue

    def sample_and_remove(self, size):
        if self.data is None:
            raise IndexError()

        n = self.data.get_num_rows()

        if size > n:
            raise IndexError()

        ret_val = {}
        for k in self.data.keys():
            rng_state = np.random.get_state()

            np.random.shuffle(self.data[k])
            split_data = np.split(self.data[k], [n-size])
            self.data[k] = split_data[0]
            ret_val[k] = split_data[1]

            # Reset the random state so that each sub-vector is shuffled in the same order.
            np.random.set_state(rng_state)

        return ret_val

