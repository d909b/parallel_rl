from distributed_memory import DistributedMemory
from multiprocessing import Array
import numpy as np


class ParameterServer():
    def __init__(self, gradient_queue, parameter_queue, initial_parameters, num_parameter_consumers):
        self._parameters = Array('d', initial_parameters)
        self._gradients = DistributedMemory(gradient_queue)
        self.parameter_queue = parameter_queue
        self.num_parameter_consumers = num_parameter_consumers

    def run(self, terminated):
        self._gradients.run(terminated)

    @property
    def parameters(self):
        return self._parameters[:]

    def update_parameters(self):
        gradients = self._gradients.get_data()
        n = len(gradients)

        if n == 0:
            return

        m = gradients[0].shape

        if m == 0:
            return

        self._parameters = (np.array(self.parameters)
                            + np.average(np.concatenate(gradients).reshape([n, m]), axis=1)).tolist()
        self._gradients.clear()