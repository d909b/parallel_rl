from Queue import Empty, Full


class Learner():
    def __init__(self, mini_batch_queue, gradient_queue, learn_func):
        self.mini_batch_queue = mini_batch_queue
        self.gradient_queue = gradient_queue
        self.learn_func = learn_func

    def run(self, terminated):
        while not terminated.value:
            try:
                batch = self.mini_batch_queue.get(timeout=1)
                gradient = self.learn_func(batch)
                self.gradient_queue.put(gradient, timeout=1)

            except Empty or Full:
                continue