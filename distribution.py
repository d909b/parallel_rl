import theano.tensor as T
import theano
import numpy as np


class Distribution():
    def sampled_variable(self):
        raise NotImplementedError

    def probabilistic_variable(self):
        raise NotImplementedError

    def likelihood(self, a, prob):
        raise NotImplementedError

    def loglikelihood(self, a, prob):
        raise NotImplementedError

    def kl_divergence(self, lprob, rprob):
        raise NotImplementedError

    def entropy(self, prob):
        raise NotImplementedError

    def sample(self, prob):
        raise NotImplementedError

    def max_value(self, prob):
        raise NotImplementedError


class CategoricalDistribution(Distribution):
    def __init__(self, n):
        self.n = n

    def sampled_variable(self):
        return T.ivector('a')

    def probabilistic_variable(self):
        return T.matrix('prob')

    def likelihood(self, a, prob):
        return prob[T.arange(prob.shape[0]), a]

    def loglikelihood(self, a, prob):
        return T.log(self.likelihood(a, prob))

    def kl_divergence(self, lprob, rprob):
        return (lprob * T.log(lprob/rprob)).sum(axis=1)

    def entropy(self, prob):
        return -(prob * T.log(prob)).sum(axis=1)

    def sample(self, prob):
        prob = np.asarray(prob)
        assert prob.ndim == self.n
        n = prob.shape[0]
        cum_dist = np.cumsum(prob, axis=1)
        return np.argmax(cum_dist > np.random.rand(n, 1), axis=1)

    def max_value(self, prob):
        return prob.argmax(axis=1)


class DiagonalGaussianDistribution(Distribution):
    def __init__(self, d):
        self.d = d

    def sampled_variable(self):
        return T.matrix('a')

    def probabilistic_variable(self):
        return T.matrix('prob')

    def loglikelihood(self, a, prob):
        mean0 = prob[:, :self.d]
        std0 = prob[:, self.d:]
        return - 0.5 * T.square((a - mean0) / std0).sum(axis=1) \
               - 0.5 * T.log(2.0 * np.pi) * self.d - T.log(std0).sum(axis=1)

    def likelihood(self, a, prob):
        return T.exp(self.loglikelihood(a, prob))

    def kl_divergence(self, lprob, rprob):
        lmean = lprob[:, :self.d]
        lstd = lprob[:, self.d:]
        rmean = rprob[:, :self.d]
        rstd = rprob[:, self.d:]
        return T.log(rstd / lstd).sum(axis=1) \
            + ((T.square(lstd) + T.square(lmean - rmean)) / (2.0 * T.square(rstd))).sum(axis=1) - 0.5 * self.d

    def entropy(self, prob):
        std = prob[:, self.d:]
        return T.log(std).sum(axis=1) + .5 * np.log(2 * np.pi * np.e) * self.d

    def sample(self, prob):
        mean_nd = prob[:, :self.d]
        std_nd = prob[:, self.d:]
        return np.random.randn(prob.shape[0], self.d).astype(theano.config.floatX) * std_nd + mean_nd

    def max_value(self, prob):
        return prob[:, :self.d]